# TEER Data Python Scripts

This folder contains all python scripts used for TEER analysis
- To use the python scripts you will need to create a local repository by cloning the remote repository
- After you create a local repository you are free to use the python code however you like
- I created virtual enviornments for all three python scripts so you don't have to re download all the needed extra stuff (pandas, scipy, matplotlib, etc.)
- I personally used microsoft visual studio to run and edit the python scripts
    - If you do it this way you will first need to open the python folder in visual studio
    - Then in the terminal window type in '.venv/Scripts/activate'
        - This will put you inisde of the virtual enviornment for that python project
    - Now you can edit and run the code. To run type in the terminal 'py python_file.py' where 'pyhon_file' is whatever file is being   run

**TEER_data:**
- This python project is used to process all the sine wave data from an experiment
- The raw sine data is collected from the Qt project and saved in a directory defined in Qt
    - You need to be sure that the sine data is in the same format as the example csv file, this is found in the example excel file folder
    - Also if you change the frequencies done in the experiment in Qt you will need to update the frequencies vector in 'convert_teer.py'
- Before running this script you need to collect all the raw sine data from the experiment and place it in a folder of your choosing
    - Preferably from oldest to newest, it should do this automatically becasue the time is in the csv title
    - there has to be nothing else in this folder other
- Then paste the directory of this folder into the variable file_path in main.py
- Now you should be able to run the code
    - The code will iterate through all the files in the director you defined and create copys in a new folder 'fitted-data'
    - These new csv files will have the same name and the raw data will be replaced with the curv fit data
    - after all the files have been converted a file fitted_data.csv will be created with the magnitude and phase data from each of the files
    - This is how you create the TEER timeline for an experimnet

-**MAKE SURE THE GAIN OF CURRENT AND VOLTAGE IS CORRECT IN THE CODE, otherwise your magnitude calculation will be wrong**

**TEER Square Wave**
- This python script works very similar to TEER_data and the input file should be in the same format as the TEER_data example
- This script is used to process the raw square wave data from Qt (has 'low current' at the end of its file name)
- Again you will need to put all the square wave data into a folder and put the directory in the 'file_path' variable
- The code will iterate through all files in the defined director and then calculate the DC magnitude for each frequency at each time point
    - It should be noted that since the DC response should be the same for each frequency of square wave
- The final DC magnitude data for each time point will be saved in a fitted_data.csv file

**Noise analysis:**
- To record the noise analysis you need to gnd the input to your circuit segment and probe the output with the scope in the lab
    - For frequency axis to be as low as 1Hz you will need to collect 1 full second worth of data.
- Save the data from the scope using 'OpenChoiceDesktop' app
- In python you will then change the variable csv_file to wherever you saved your file.
    - Make sure the file is formatted the same as the example, most likely you will need to add a row at the very top and type 'y-raw' over the y-axis data
- The code when ran will first show a plot of your power spectral density, it will also save the data from this plot in the file test.csv
- Other usefull metrics are printed to the terminal

