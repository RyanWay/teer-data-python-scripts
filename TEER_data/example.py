import pandas as pd
import numpy, scipy.optimize
import pylab as plt
import fit_sin

csv_file = 'test.csv'

df_csv = pd.read_csv(csv_file)
# df_csv.dropna(inplace = True)

#define x and y vectors to fit
tt = df_csv['Raw Current X Freq:12 time']
yy = df_csv['Raw Current Y Freq:12']
tt_volt = df_csv['Raw Voltage X Freq10000 time']
yy_volt = df_csv['Raw Voltage Y Freq10000']
tt_hiFreq1 = df_csv['Raw Current X Freq:10000 time']
yy_hiFreq1 = df_csv['Raw Current Y Freq:10000']
tt_hiFreq = df_csv['Raw Current X Freq:30000 time']
yy_hiFreq = df_csv['Raw Current Y Freq:30000']

tt_volt.dropna(axis=0, how='any', inplace=True)
yy_volt.dropna(axis=0, how='any', inplace=True)

#call the fit_sin function on x and y vectors (tt,yy)
res = fit_sin.fit_sin(tt_volt, yy_volt)
print( "Amplitude=%(amp)s, Angular freq.=%(omega)s, phase=%(phase)s, offset=%(offset)s, Max. Cov.=%(maxcov)s" % res )

plt.plot(tt_volt, yy_volt, "-k", label="y", linewidth=2)
plt.plot(tt_volt, res["fitfunc"](tt_volt), "r-", label="y fit curve", linewidth=2)
plt.legend(loc="best")
plt.show()