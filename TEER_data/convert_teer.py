import pandas as pd
import numpy as np, scipy.optimize
import pylab as plt
import fit_sin
import csv
import os

def convert_teer(csv_file, read_path, dest_path, file_num):
    # csv_file = 'TEER-Ch0--2023-07-25 12_21_05-low-current.csv'
    # csv_file = 'TEER-Ch0--2023-07-24 12_09_04-high-current.csv'
    # freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000','15000','20000','30000']
    freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000']
    df_csv = pd.read_csv(read_path+'/'+csv_file, on_bad_lines='skip')

    df_final = pd.DataFrame({csv_file: ['']})
    # df_new.to_csv('test1.csv')
    loc = 0
    for x in freq:
        time_col = 'Raw Current X Freq:'+ x +' time'
        current_col = 'Raw Current Y Freq:'+ x
        voltage_col = 'Raw Voltage Y Freq'+ x
        df_temp = pd.DataFrame(df_csv[[time_col,current_col,voltage_col]])
        # print(df_temp)
    #drop unused rows
        df_temp.drop(df_temp[df_temp[current_col] == ' '].index, inplace=True)
        df_temp.drop(df_temp[df_temp[time_col] == ' '].index, inplace=True)
        df_temp.drop(df_temp[df_temp[voltage_col] == ' '].index, inplace=True)
    #convert columns to appropriate data type
        df_temp[time_col] = pd.to_numeric(df_temp[time_col])
        df_temp[current_col] = pd.to_numeric(df_temp[current_col])
        df_temp[voltage_col] = pd.to_numeric(df_temp[voltage_col])
        
        df_temp[current_col] = scipy.signal.medfilt(df_temp[current_col], 21)
        df_temp[voltage_col] = scipy.signal.medfilt(df_temp[voltage_col], 21)
    
    #combine with final dataframe
        df_final = pd.concat([df_final, df_temp], axis=1)

    TIA_gain = 27000
    INA_gain = 9.8214

    df_teer = pd.DataFrame({csv_file:[], 'Frequency-'+str(file_num):[], 'Magnitude-'+str(file_num):[], 'Phase-'+str(file_num):[]})
    # df_teer = pd.DataFrame({csv_file:[], 'Frequency-'+str(file_num):[], 'Magnitude-'+str(file_num):[], 'Phase-'+str(file_num):[], 'Current Phase-'+str(file_num):[], 'Voltage Phase-'+str(file_num):[]})
    df_data = pd.DataFrame({csv_file:[]})
    for i in freq:
        time_col = 'Raw Current X Freq:'+ i +' time'
        current_col = 'Raw Current Y Freq:'+ i
        voltage_col = 'Raw Voltage Y Freq'+ i
        poly_col = 'Voltage Polynomial Fit: '+ i

        tt_volt = df_final[time_col]
        yy_current = df_final[current_col]
        yy_volt = df_final[voltage_col]

        #Remove any NaN values in list
        tt_volt.dropna(axis=0, how='any', inplace=True)
        yy_volt.dropna(axis=0, how='any', inplace=True)
        yy_current.dropna(axis=0, how='any', inplace=True)
        
        #reduce size of list to 1/3 of total 
        tt_volt = tt_volt.iloc[:round(len(tt_volt)/2)]
        yy_volt = yy_volt.iloc[:round(len(yy_volt)/2)]
        # yy_volt = yy_volt.iloc[:round(len(tt_volt))]
        yy_current = yy_current.iloc[:round(len(yy_current)/2)]
        # yy_current = yy_current.iloc[:round(len(tt_volt))]

        #fit a 1st degree polynomial to curve, this is to track the DC shift down over time
        print('freq = '+i)
        print('tt_volt='+str(len(tt_volt))+' yy_volt='+str(len(yy_volt)))
        poly = np.poly1d(np.polyfit(tt_volt,yy_volt,1))
        y_poly = poly(tt_volt)
        yy_volt = yy_volt-y_poly

        #Call the fit_sin() function for current and voltage and calculate phase and magnitude
        res_volt = fit_sin.fit_sin(tt_volt, yy_volt)
        res_current = fit_sin.fit_sin(tt_volt,yy_current)
        TEER_phase = res_volt["phase"]*180/np.pi-res_current["phase"]*180/np.pi
        TEER_mag = res_volt["amp"]/res_current["amp"]*(TIA_gain/INA_gain)
        if(TEER_mag < 0):
            TEER_mag = TEER_mag*-1
            TEER_phase = (180-TEER_phase)*-1
        df_temp = pd.DataFrame({'Frequency-'+str(file_num):[i], 'Magnitude-'+str(file_num):[TEER_mag], 'Phase-'+str(file_num):[TEER_phase]})
        # df_temp = pd.DataFrame({'Frequency-'+str(file_num):[i], 'Magnitude-'+str(file_num):[TEER_mag], 'Phase-'+str(file_num):[TEER_phase], 'Current Phase-'+str(file_num):[res_current["phase"]*180/np.pi], 'Voltage Phase-'+str(file_num):[res_volt["phase"]*180/np.pi]})
        df_teer = pd.concat([df_teer,df_temp], ignore_index=True)

        volt_fit = list(map(res_volt['fitfunc'], tt_volt))
        current_fit = list(map(res_current['fitfunc'], tt_volt))
        # print(volt_fit)
        df_temp_data = pd.DataFrame(list(zip(tt_volt, current_fit, volt_fit)), columns=[time_col, current_col, voltage_col])
        df_temp_data2 = pd.DataFrame(list(zip(tt_volt, current_fit, volt_fit, y_poly)), columns=[time_col, current_col, voltage_col, poly_col])
        # df_temp_data = pd.DataFrame({time_col: [tt_volt], current_col: [current_fit], voltage_col: [volt_fit]})
        df_data = pd.concat([df_data, df_temp_data2], axis = 1, ignore_index=True)
    os.makedirs(dest_path, exist_ok=True)
    df_data.to_csv(dest_path + csv_file)
    return df_teer





