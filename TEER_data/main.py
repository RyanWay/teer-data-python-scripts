import pandas as pd
import numpy as np, scipy.optimize
import pylab as plt
import fit_sin
import csv
import os
import convert_teer

file_path = "C:/Users/ryanw/Desktop/TEER/Ch1"
# file_path = "T:/gaussi/Lab_Projects/Smart_Well_Plate/USSING Chamber/TEER/Chambers 1, 2, and 3 (7_24_23)/Chamber2/Low Current"
chamber_num = file_path.removeprefix("C:/Users/ryanw/Desktop/TEER/")
df_final = pd.DataFrame({chamber_num: []})
file_num = 0
for files in os.listdir(file_path):
    file_num = file_num + 1
    csv_file = files
    # dest_path = 'C:/Users/ryanw/Desktop/TEER/Test curve fit'
    os.makedirs(file_path + '/fitted-data', exist_ok=True)
    dest_path = file_path + '/fitted-data/'
    df_temp = convert_teer.convert_teer(csv_file, file_path, dest_path, file_num)
    df_final = pd.concat([df_final,df_temp], axis=1)
print(df_final)
df_final.to_csv(file_path+'/fitted-data/fitted-data.csv')

