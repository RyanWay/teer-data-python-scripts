import pandas as pd
import numpy, scipy.optimize
import pylab as plt
import fit_sin
import csv

csv_file = 'TEER-Ch0--2023-07-25 12_21_05-low-current.csv'
freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000','15000','20000','30000']
df_csv = pd.read_csv(csv_file)
print(df_csv['Raw Current X Freq:12 time'].shape[0])

#remove all columns after the raw data
df_csv.drop(df_csv.loc[:,'Shifted_voltage Freq:12':'Phase2'],inplace=True, axis=1)

#removes all rows that are equal to ' '
df_csv.drop(df_csv[df_csv['Raw Current Y Freq:12'] == ' '].index, inplace=True)

print(df_csv['Raw Voltage Y Freq12'].shape[0])


#iterate through all headers in csv file
# for x in df_csv.head(0):
#     if x == 'Shifted_voltage Freq:12':
#         flag = True
#     if flag == True:
#         df_csv.drop(x, inplace=True, axis=1

#read in single column from CSV
df_new = pd.read_csv(csv_file, usecols = ['Raw Voltage Y Freq12'])
print(df_new['Raw Voltage Y Freq12'].dtypes)
print(df_new['Raw Voltage Y Freq12'].shape[0])

#Drop unused rows from that column
df_new.drop(df_new[df_new['Raw Voltage Y Freq12'] == ' '].index, inplace=True)
#convert the rest of column to numeric data type
df_new['Raw Voltage Y Freq12'] = pd.to_numeric(df_new['Raw Voltage Y Freq12'])

print(df_new['Raw Voltage Y Freq12'].dtypes)
print(df_new['Raw Voltage Y Freq12'].shape[0])

df_new.to_csv('test1.csv')
