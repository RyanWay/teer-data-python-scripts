import pandas as pd
import numpy as np, scipy.optimize
import pylab as plt
import csv
import os
from scipy.fft import fft, ifft
import scipy.integrate as integrate
import math
import array as arr

# csv_file = 'ch1-current-1ms.csv'
csv_file = 'C:/Users/ryanw/OneDrive - Colostate/USSING Chamber Research Files/Thesis Documents/Finalized Data/System Verification/Noise/noise2/ch1-current-100ms-scope2.csv'
df_csv = pd.read_csv(csv_file)
# print(df_csv)

x_raw = df_csv.iloc[:,3]
# y_raw = df_csv['y-raw']
y_raw = df_csv.iloc[:,4]
fs = 1/float(df_csv.iloc[1,1])
length = len(y_raw)
voltage_gain = 9.8214
current_gain = 27000
y_raw = y_raw/voltage_gain
# y_raw = y_raw/current_gain


noise_power = y_raw**2
avg_noise_power = integrate.simps(noise_power)/length #calculates total noise power (V^2) in the time domain
# print('Average Noise Power: '+str(avg_noise_power*1000000)+'uV^2')


#### Calculating the Power Spectral Density #####
y_fft = fft(np.array(y_raw))
scaling_factor = (1/fs*1/length)
psd = abs(y_fft)**2*scaling_factor #|fft|^2*scaling_factor
psd = 2*psd # one sided signal requires the data to be multiplied by 2
f = np.fft.fftfreq(len(x_raw),1/fs) # frequency axis np.fft.fftfreq(length of time axis,time per sample)
print(len(f))
plt.xscale("log")
plt.yscale("log")
plt.xlabel('Frequency(Hz)')
plt.ylabel('Power Spectral Density $\mathregular{nV^{2}}$/Hz', weight = 'bold')
plt.title('Read Channel Noise Spectrum', weight = 'bold')
plt.plot(f[1:f.size//2], psd[1:f.size//2]*10**9, color='red')
plt.grid(which = 'both', axis = 'both')
plt.minorticks_on()
plt.tick_params(which = "minor", bottom = False, left = False)
plt.tight_layout()
plt.savefig('noise_sepctrum.png', bbox_inches='tight', dpi=300) # dots per inch
plt.show()


data = {'freq': f[1:f.size//2], 'psd': psd[1:f.size//2]}
psd_dataframe = pd.DataFrame(data)
psd_dataframe.to_csv('test.csv')
#################################################



###### Calcualting the total power (integrated PSD up till bandwidth) ################
bandwidth = 47500
i = 0
for x in f:
    i=i+1
    if x==bandwidth:
        f_BW=f[0:i]
        break

total_noise_power = scipy.integrate.simps(psd[0:f_BW.size],f_BW)
print('Total Noise Power(uV^2): '+str(total_noise_power*10**6))
######

#### calculate average spectral density up till BW #######
psd_temp = 0
for y in range(len(f_BW)):
    psd_temp = psd_temp + math.sqrt(psd[y])
    # psd_temp = psd_temp + psd[y]*fs/length

psd_avg = psd_temp/len(f_BW)
# psd_avg = math.sqrt(psd_temp)/len(f_BW)

print('Average Spectral Density (nV/sqrt(Hz)): '+str(psd_avg*10**9))
#######

#### calculate spot noise @ 1k, 10k, 100k #######
index = 0
for z in f:
    index = index + 1
    if z==100:
        spotnoise100 = math.sqrt(psd[index])
        print(psd[index])
    if z==1000:
        spotnoise1k = math.sqrt(psd[index])
    if z==10000:
        spotnoise10k = math.sqrt(psd[index])

print('Spot Noise 100Hz (nV/sqrt(Hz): '+str(spotnoise100*10**9))
print('Spot Noise 1kHz (nV/sqrt(Hz): '+str(spotnoise1k*10**9))
print('Spot Noise 10kHz (nV/sqrt(Hz): '+str(spotnoise10k*10**9))
