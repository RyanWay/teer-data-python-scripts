import pandas as pd
import numpy as np, scipy.optimize
import pylab as plt
import csv
import os
import square_wave_mag

# file_path = "E:/Chamber 4/square"
file_path = "C:/Users/ryanw/Desktop/TEER/Ch5"
chamber_num = file_path.removeprefix("C:/Users/ryanw/Desktop/TEER/")
df_final = pd.DataFrame({chamber_num: []})
file_num = 0
for files in os.listdir(file_path):
    file_num = file_num + 1
    csv_file = files
    # dest_path = 'C:/Users/ryanw/Desktop/TEER/Test curve fit'
    os.makedirs(file_path + '/fitted-data', exist_ok=True)
    dest_path = file_path + '/fitted-data/'
    df_temp = square_wave_mag.square_wave_mag(csv_file, file_path, dest_path, file_num)
    df_final = pd.concat([df_final,df_temp], axis=1)
print(df_final)
df_final.to_csv(file_path+'/fitted-data/fitted-data.csv')

