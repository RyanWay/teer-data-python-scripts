import pandas as pd
import numpy as np, scipy.optimize, scipy.signal
import pylab as plt
import csv
import os
from scipy.fft import fft, ifft


def square_wave_mag(csv_file, read_path, dest_path, file_num):
    # csv_file = 'TEER-Ch0--2023-08-14 14_13_41-low-current.csv'
    
    df_csv = pd.read_csv(read_path+'/'+csv_file, on_bad_lines='skip', encoding = "utf-8")

    # freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000','15000','20000','30000']
    freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000']
    
    df_final = pd.DataFrame({csv_file: ['']})

    for x in freq:
        time_col = 'Raw Current X Freq:'+ x +' time'
        current_col = 'Raw Current Y Freq:'+ x
        voltage_col = 'Raw Voltage Y Freq'+ x
        df_temp = pd.DataFrame(df_csv[[time_col,current_col,voltage_col]])
    #drop unused rows
        df_temp.drop(df_temp[df_temp[current_col] == ' '].index, inplace=True)
        df_temp.drop(df_temp[df_temp[time_col] == ' '].index, inplace=True)
        df_temp.drop(df_temp[df_temp[voltage_col] == ' '].index, inplace=True)
    #convert columns to appropriate data type
        df_temp[time_col] = pd.to_numeric(df_temp[time_col])
        df_temp[current_col] = pd.to_numeric(df_temp[current_col])
        df_temp[voltage_col] = pd.to_numeric(df_temp[voltage_col])

    #subtract the average from current and voltage list
        df_temp.dropna(axis=0, how='any', inplace=True)
        df_temp[current_col] = df_temp[current_col] - np.average(df_temp[current_col])
        df_temp[voltage_col] = df_temp[voltage_col] - np.average(df_temp[voltage_col])

        df_temp[current_col] = scipy.signal.medfilt(df_temp[current_col], 31)
        df_temp[voltage_col] = scipy.signal.medfilt(df_temp[voltage_col], 31)
        # print(np.average(df_temp[voltage_col]))
    #combine with final dataframe
        df_final = pd.concat([df_final, df_temp], axis=1)
    # df_final.to_csv("test1.csv")

    TIA_gain = 27000
    INA_gain = 9.8
    file_num = 0
    df_teer = pd.DataFrame({csv_file:[], 'Frequency-'+str(file_num):[], 'Magnitude-'+str(file_num):[], 'Volt-'+str(file_num):[], 'Current-'+str(file_num):[]})
    df_data = pd.DataFrame({csv_file:[]})
    for i in freq:
        time_col = 'Raw Current X Freq:'+ i +' time'
        current_col = 'Raw Current Y Freq:'+ i
        voltage_col = 'Raw Voltage Y Freq'+ i    

        dx_temp = df_final[time_col].dropna(axis=0, how='any')
        dy_temp = df_final[voltage_col].dropna(axis=0, how='any')
        dy_temp_curr = df_final[current_col].dropna(axis=0, how='any')

        period = 1/float(i)*1000
        time_per_samp = dx_temp.iloc[0]
        samples_per_period = period/time_per_samp

        dx_temp = np.array(dx_temp)
        dy_temp = np.array(dy_temp)
        dy_temp_curr = np.array(dy_temp_curr)
        dx_temp = dx_temp[int(samples_per_period/2):] # delete half of the first period
        dy_temp = dy_temp[int(samples_per_period/2):] # delete half of the first period
        dy_temp_curr = dy_temp_curr[int(samples_per_period/2):] # delete half of the first period

        
    # take derivative and find switching points for voltage
        dydx_temp = np.diff(dy_temp)/np.diff(dx_temp) # take derivative
        max_index = scipy.signal.argrelextrema(dydx_temp, np.greater, order = int(samples_per_period/1.5)) # return an array of the spike indexes
        max_index = np.array(max_index)
        print(max_index.shape[1])
        first_spike = max_index[:,[1]]
        if(max_index.shape[1] > 2):
            second_spike = max_index[:,[2]]
        else:
            second_spike = first_spike        
        if(max_index.shape[1] > 3):
            third_spike = max_index[:,[3]]
        else:
            third_spike = second_spike
        if(max_index.shape[1] > 4):
            fourth_spike = max_index[:,[4]]
        else:
            fourth_spike = third_spike
        
        low_point1 = dy_temp[first_spike-2]
        high_point1 = dy_temp[first_spike+2]
        low_point2 = dy_temp[second_spike-2]
        high_point2 = dy_temp[second_spike+2]
        low_point3 = dy_temp[third_spike-2]
        high_point3 = dy_temp[third_spike+2]
        low_point4 = dy_temp[fourth_spike-2]
        high_point4 = dy_temp[fourth_spike+2]

        diff1 = high_point1-low_point1
        diff2 = high_point2-low_point2
        diff3 = high_point3-low_point3
        diff4 = high_point4-low_point4

        diff = np.average([diff1,diff2,diff3,diff4])
    # take derivative and find switching points for Current
        dydx_temp_curr = np.diff(dy_temp_curr)/np.diff(dx_temp) # take derivative
        max_index_curr = scipy.signal.argrelextrema(dydx_temp_curr, np.greater, order = int(samples_per_period/1.5)) # return an array of the spike indexes
        max_index_curr = np.array(max_index_curr)
        print(max_index_curr)
        first_spike_curr = max_index_curr[:,[1]]
        low_point_curr = dy_temp_curr[first_spike_curr-10]
        high_point_curr = dy_temp_curr[first_spike_curr+10]

        current_mag = np.abs(high_point_curr-low_point_curr)/TIA_gain #calculate the current magnitude and divide by gain
        voltage_mag = np.abs(diff)/INA_gain #calculate the voltage magnitude and divide by gain

        TEER_mag = voltage_mag/current_mag #calcualte the TEER magnitude (Z = V/I)

        print(first_spike)
        print('Low Point ='+ str(low_point1))
        print('High Point ='+ str(high_point1))
        print('Voltage ='+str(voltage_mag))
        print('Current ='+str(current_mag))
        print('TEER ='+str(TEER_mag))
        df = pd.DataFrame({'Frequency-'+str(file_num):[i], 'Magnitude-'+str(file_num):[float(TEER_mag)]})
        df_teer = pd.concat([df_teer,df], ignore_index=True)
    os.makedirs(dest_path, exist_ok=True)
    return df_teer



