import pandas as pd
import numpy as np, scipy.optimize, scipy.signal
import pylab as plt
import csv
import os
from scipy.fft import fft, ifft

# csv_file = 'TEER-Ch0--2023-08-21 14_16_10-low-current.csv'
csv_file = 'TEER-Ch0--2023-08-23 08_18_55-low-current.csv'
df_csv = pd.read_csv(csv_file)

freq = ['12','44','76','108','140','172','204','236','268','300','332','364','396','428','750','1000','3000','4000','5000','10000','15000','20000','30000']

df_final = pd.DataFrame({csv_file: ['']})

for x in freq:
    time_col = 'Raw Current X Freq:'+ x +' time'
    current_col = 'Raw Current Y Freq:'+ x
    voltage_col = 'Raw Voltage Y Freq'+ x
    df_temp = pd.DataFrame(df_csv[[time_col,current_col,voltage_col]])
#drop unused rows
    df_temp.drop(df_temp[df_temp[current_col] == ' '].index, inplace=True)
    df_temp.drop(df_temp[df_temp[time_col] == ' '].index, inplace=True)
    df_temp.drop(df_temp[df_temp[voltage_col] == ' '].index, inplace=True)
#convert columns to appropriate data type
    df_temp[time_col] = pd.to_numeric(df_temp[time_col])
    df_temp[current_col] = pd.to_numeric(df_temp[current_col])
    df_temp[voltage_col] = pd.to_numeric(df_temp[voltage_col])

#subtract the average from current and voltage list
    df_temp.dropna(axis=0, how='any', inplace=True)
    df_temp[current_col] = df_temp[current_col] - np.average(df_temp[current_col])
    df_temp[voltage_col] = df_temp[voltage_col] - np.average(df_temp[voltage_col])

    # df_temp[current_col] = scipy.signal.medfilt(df_temp[current_col], 5)
    # df_temp[voltage_col] = scipy.signal.medfilt(df_temp[voltage_col], 5)
    # print(np.average(df_temp[voltage_col]))
#combine with final dataframe
    df_final = pd.concat([df_final, df_temp], axis=1)
df_final.to_csv("test1.csv")

TIA_gain = 27000
INA_gain = 9.8
# for i in freq:
#     time_col = 'Raw Current X Freq:'+ i +' time'
#     current_col = 'Raw Current Y Freq:'+ i
#     voltage_col = 'Raw Voltage Y Freq'+ i
    
#     period = 1/float(i)*1000
#     time_per_samp = df_final[time_col].iloc[0]
#     samples_per_period = period/time_per_samp
#     # print(1.25*samples_per_period)

#     current_mag = df_final[current_col].iloc[int(1.25*samples_per_period)]-df_final[current_col].iloc[int(1.25*samples_per_period+0.5*samples_per_period)]
#     voltage_mag = df_final[voltage_col].iloc[int(1.25*samples_per_period)]-df_final[voltage_col].iloc[int(1.25*samples_per_period+0.5*samples_per_period)]
#     TEER_mag = voltage_mag/current_mag*(TIA_gain/INA_gain)
#     print(TEER_mag)

# calculate the derivative and also find the zero crossing points
dx = df_final['Raw Current X Freq:12 time'].dropna(axis=0, how='any')
dy = df_final['Raw Voltage Y Freq12'].dropna(axis=0, how='any')

# take derivative
dydx = np.diff(dy)/np.diff(dx)

# find zero crossings
zero_crossing = np.where(np.diff(np.sign(dy)))[0] 

period = 1/12*1000
time_per_samp = dx.iloc[0]
samples_per_period = period/time_per_samp
dydx = np.array(dydx)
dydx = dydx[int(samples_per_period/2):] # delete half of the first period
max_index = scipy.signal.argrelextrema(dydx, np.greater, order = int(samples_per_period/1.5)) # return an array of the spike indexes

# df_index = df.iloc[df[df > 4000].index] 
print(max_index)
# dydx = scipy.signal.medfilt(dydx, 5)
df = pd.DataFrame(dydx)
df.to_csv('derivative1.csv')


x_data = df_final['Raw Current X Freq:44 time'].dropna(axis=0, how='any')
y_data = df_final['Raw Voltage Y Freq44'].dropna(axis=0, how='any')


dt = x_data.iloc[0]/1000
t = np.arange(0, y_data.shape[-1])*dt
y = fft(np.array(y_data))/t.shape[0]
print(y)
freq = np.fft.fftfreq(t.shape[0], dt)
# freq_axis_pos = freq[0: np.argmax(freq < 0)]
freq_axis_pos = freq[0:np.argmax(freq > 200000)]
y_pos = 2*y[0:np.argmax(freq > 200000)]

plt.figure()
plt.plot(freq_axis_pos,np.abs(y_pos))
plt.xscale('log')
plt.show()
